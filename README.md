# Description

This project demonstrates the usage of reactive functions for Javascript:
- `Promise`
- `async`, `await`
- and all [reactive extensions](https://rxjs-dev.firebaseapp.com/)

# Requirements

Node >= 10 must be correctly installed.
Nvm is a good choice but it's not mandatory.

# "Build"

```bash
npm install
```

# Run

```bash
node src/no-primises.js
node src/promises.js
node src/async-await.js
```
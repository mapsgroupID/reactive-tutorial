'use strict';

const { interval } = require('rxjs');
const { filter, scan } = require('rxjs/operators');

function an_observer(sym) {
  return n => console.log(`${sym} Got ${n}`);
}

// `interval` emits a number every N milliseconds
const o = interval(500);

const q = o.pipe(
  filter(x => x % 2 === 0),
  scan((acc, n) => acc + n, 100)
);

q.subscribe(an_observer('---'));
q.subscribe(an_observer('+++'));
q.subscribe(an_observer('~~~'));

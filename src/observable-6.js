'use strict';

const { interval } = require('rxjs');
const { filter, scan } = require('rxjs/operators');

function an_observer(sym) {
  return {
    next: n => console.log(`${sym} Got ${n}`),
    error: err => console.log(`${sym} Err: ${err}`),
    complete: () => console.log(`${sym} Completed`)
  };
}

const o = interval(500);

const q = o.pipe(
  filter(x => x % 2 === 0),
  scan((acc, n) => acc + n, 100)
);

const sub1 = q.subscribe(an_observer('---'));
const sub2 = q.subscribe(an_observer('+++'));
const sub3 = q.subscribe(an_observer('~~~'));

setTimeout(() => {
  [sub1, sub2, sub3].forEach(s => s.unsubscribe());
}, 5000);

setTimeout(() => {
  const sub = q.subscribe(an_observer('!!!'));
  setTimeout(() => {
    sub.unsubscribe();
  }, 10000);
}, 8000);

'use strict';

const { Subject } = require('rxjs');
const { filter, map, scan } = require('rxjs/operators');

function an_observer(sym) {
  return n => console.log(`${sym}${n}`);
}

// a subject can generate new values using subject.next(value)
const subject = new Subject();

// emission is scheduled every 300 ms
const intHdlr = setInterval(() => {
  const n = Math.floor(Math.random() * 101); // 0-100
  subject.next(n);
}, 300);

// stops emission in 5 seconds
setTimeout(() => clearInterval(intHdlr), 5000);

// a subject is an observable
const o = subject.pipe(filter(x => x % 3 === 0));

// we can subscribe to the observable as-is
o.subscribe(an_observer('x = '));

// or to derived observables
o.pipe(map(x => x * x)).subscribe(an_observer('x^2 = '));
o.pipe(scan((acc, n) => acc + n, 0)).subscribe(an_observer('Σ(x) = '));

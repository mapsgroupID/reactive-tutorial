'use strict';

const { listCharacters, getRole, getPlace } = require('./async-promise-apis');

async function f() {
  try {
    const chars = await listCharacters(10);
    chars.forEach(async ch => {
      const [userRole, userPlace] = await Promise.all([getRole(ch), getPlace(ch)]);
      console.log(`${ch.name} ${ch.surname} is ` +
                  `${userRole.role} at ${userPlace.place}`);
    });
  } catch (err) {
    console.error(`Ops... ${err}`);
  }
}

f();

'use strict';

const { Observable } = require('rxjs');

function an_observer(sym) {
  return n => console.log(`${sym}${n}`);
}

function genChar() {
  const CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWZ';
  let i = 0;

  return () => {
    const c = CHARS[i];
    i = (i + 1) % CHARS.length;
    return c;
  };
}

function genNum() {
  let n = 0;
  return () => n++;
}

function subscribe(subscriber) {
  const isNum = Math.round(Math.random());
  const generator = isNum === 1 ? genNum() : genChar();
  let count = Math.floor(Math.random() * 10) + 1;
  console.log(`New subscriber prepared: numeric=${isNum === 1 ? 'y' : 'n'}, count=${count}`);

  const intId = setInterval(() => {
    count--;
    const value = generator();
    subscriber.next(value);
    if (count === 0) {
      subscriber.complete();
    }
  }, 100);

  return function unsubscribe() {
    clearInterval(intId);
  };
}

// an observable can be created from a subscribe function
const obs = new Observable(subscribe);

// we can subscribe to the observable as-is
obs.subscribe(an_observer('!!! '));
obs.subscribe(an_observer('*** '));

'use strict';

const { mkRandomCharacters, mkRandomPlace, mkRandomRole } = require('./data');

function listCharacters(count, callback) {
  setTimeout(() => {
    const chars = mkRandomCharacters(count);
    callback(chars);
  }, 500);
}

function getRole(char, callback) {
  setTimeout(() => {
    const role = mkRandomRole();
    callback({ char, role });
  }, 500);
}

function getPlace(char, callback) {
  setTimeout(() => {
    const place = mkRandomPlace();
    callback({ char, place });
  }, 500);
}

listCharacters(10, chars => {
  chars.forEach(char => {
    getRole(char, charRole => {
      getPlace(char, charPlace => {
        console.log(`${char.name} ${char.surname} is ${charRole.role} at ${charPlace.place}`);
      });
    });
  });
});

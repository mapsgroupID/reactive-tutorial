'use strict';

const { mkRandomCharacters, mkRandomPlace, mkRandomRole } = require('./data');

function listCharacters(count) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const chars = mkRandomCharacters(count);
      resolve(chars);
    }, 500);
  });
}

function getRole(char) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const role = mkRandomRole();
      resolve({ char, role });
    }, 500);
  });
}

function getPlace(char) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const place = mkRandomPlace();
      resolve({ char, place });
    }, 500);
  });
}

module.exports = {
  listCharacters,
  getRole,
  getPlace
};

'use strict';

const { Observable } = require('rxjs');

function an_observer(sym) {
  return n => console.log(`${sym}${n}`);
}

function genChar() {
  const CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWZ';
  let i = 0;

  return () => {
    const c = CHARS[i];
    i = (i + 1) % CHARS.length;
    return c;
  };
}

function genNum() {
  let n = 0;
  return () => n++;
}

function subscribe() {
  const isNum = Math.round(Math.random());
  const generator = isNum === 1 ? genNum() : genChar();
  const subs = [];
  let count = 100;
  console.log(`New subscriber prepared: numeric=${isNum === 1 ? 'y' : 'n'}, count=${count}`);

  const intId = setInterval(() => {
    count--;
    const value = generator();
    if (subs.length) {
      subs.forEach(s => {
        s.next(value);
        if (count === 0) {
          s.complete();
        }
      })
    }
  }, 500);

  return (subscriber) => {
    subs.push(subscriber);
    return function unsubscribe() {
      const idx = subs.indexOf(subscriber);
      if (idx >= 0) {
        subs.splice(idx, 1);
        if (subs.length === 0) {
          clearInterval(intId);
        }
      }
    };
  }
}

// an observable can be created from a subscribe function
const obs = new Observable(subscribe());

// we can subscribe to the observable as-is
obs.subscribe(an_observer('1: '));
obs.subscribe(an_observer('2: '));
setTimeout(() => obs.subscribe(an_observer('3: ')), 2000);
setTimeout(() => obs.subscribe(an_observer('4: ')), 3000);

'use strict';

const { aGameName, aGameType, anAge } = require('./data');

class Player {
  constructor(age, favGames) {
    this.age = age;
    this.favGames = favGames;
  }
}

class Game {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }
}

function mkPlayer() {
  const age = anAge();
  const nr = Math.floor(Math.random() * 5);
  const games = [];
  for (let i = 0; i < nr; i++) {
    games.push(new Game(aGameName(), aGameType()));
  }
  return new Player(age, games);
}

const playersNr = 1 + Math.floor(Math.random() * 10);
const players = [];
for (let i = 0; i < playersNr; i++) {
  players.push(mkPlayer());
}

const arr = players.flatMap(p => p.favGames.map(g => ({ game: g, age: p.age })));
console.log(arr);

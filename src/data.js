'use strict';

class Character {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
  }
}

function mkRandomCharacters(n) {
  const arr = [];
  for (let i = 0; i < n; i++) {
    const c = new Character(aName(), aSurname());
    arr.push(c);
  }
  return arr;
}

const ROLES = ["evil", "good", "neutral"];
const PLACES = ["Winterfell", "Dragon Stone", "King's Landing", "Castle Black"];
const NAMES = ["John", "Arya", "Danaeris", "Rob", "Rhaegar", "Ned", "Cercei", "Tyrion", "Jamie", "Sam"];
const SURNAMES = ["Snow", "Targeryan", "Stark", "Lannister"];
const GAME_NAMES = ["Monopoli", "Fifa", "D&D", "Soccer", "Skate", "Baseball", "Volleyball", "Counter Strike"];
const GAME_TYPES = ["OUTDOOR", "CONSOLE", "TABLETOP"];

const aName = randomly(NAMES);
const aSurname = randomly(SURNAMES);

function anAge() {
  return 5 + Math.floor(Math.random() * 60);
}

function randomly(arr) {
  return () => {
    const i = Math.floor(Math.random() * arr.length);
    return arr[i];
  };
}

module.exports  = {
  mkRandomCharacters,
  mkRandomRole: randomly(ROLES),
  mkRandomPlace: randomly(PLACES),
  aName,
  aSurname,
  anAge,
  aGameName: randomly(GAME_NAMES),
  aGameType: randomly(GAME_TYPES)
};

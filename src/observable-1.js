'use strict';

const { of } = require('rxjs');
const { filter, scan } = require('rxjs/operators');

function an_observer(n) {
  console.log(`>>> Got ${n}`);
}

// `of` creates an observable out of a list of values
const o = of(1, 2, 3, 4, 5, 6, 7, 8, 9);

// we use `.pipe` to declare a transformation pipeline
const q = o.pipe(
  filter(x => x % 2 === 0),
  scan((acc, n) => acc + n, 100)
);

// nothing happens until observable is subscribed
// not even the "asynchronous" operation that backs it
q.subscribe(an_observer);

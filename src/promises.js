'use strict';

const { listCharacters, getRole, getPlace } = require('./async-promise-apis');

listCharacters(10).then(chars => {
  chars.forEach(char => {
    const res = [getRole(char), getPlace(char)];
    Promise.all(res).then(rolePlace => {
      console.log(`${char.name} ${char.surname} is ${rolePlace[0].role} at ${rolePlace[1].place}`);
    });
  });
});

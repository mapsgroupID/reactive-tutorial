'use strict';

const { interval } = require('rxjs');
const { filter, tap } = require('rxjs/operators');

function an_observer(sym) {
  return n => console.log(`${sym}${n}`);
}

const o = interval(100).pipe(
  tap(x => console.log(`Emitted ${x}`)),
  filter(x => x % 2 === 0)
);

console.log('Nothing is going to happen in 10 seconds...');
setTimeout(() => {
  console.log("I'm subscribing for just 1 second...");
  const s = o.subscribe(an_observer('x is even = '));
  setTimeout(() => {
    s.unsubscribe();
    console.log("No longer subscribed!");
  }, 1000);
}, 10000);
